---
en:
  vagrant:
    commands:
      status:
        frozen: |-
          The VM is frozen. To start it, you can run `vagrant up`, or you can
          instead run `vagrant destroy` to delete it and discard all saved state.
        stopped: |-
          The VM is stopped. To start it, you can run `vagrant up`, or you can
          instead run `vagrant destroy` to delete it and discard all saved state.
    actions:
      vm:
        snapshot:
          not_found: |-
            The machine has no snapshot named '%{name}'.
          restored: |-
            The machine has been restored to snapshot '%{name}'!
    help:
      shadow: |-
        In order to use shared folders, you must first add the effective user
        and group ID (which are currently %{uid} and %{gid}) to the host machine's
        subuid(5) and subgid(5) files:
        
            $ echo root:%{uid}:1 | sudo tee -a /etc/subuid
            $ echo root:%{gid}:1 | sudo tee -a /etc/subgid
        
        For more information about these commands, and user/group ID mapping in
        general, refer to this article:
        
            https://insights.ubuntu.com/2017/06/15/custom-user-mappings-in-lxd-containers/
        
    errors:
      lxd_connection_failure: |-
        The LXD provider was unable to contact the daemon at %{api_endpoint}.
        
        It's possible that LXD isn't installed, or that it isn't configured to
        accept HTTPS connections from your machine. You can check whether HTTPS
        access is enabled with the following command:
        
            $ lxc config get core.https_address
        
        If the result is empty or an error is shown, you will need to correct
        the way LXD is configured before Vagrant can use it. This can be done
        with the following command:
        
            $ lxc config set core.https_address %{https_address}
        
        You can find more documentation about configuring LXD at:
        
            https://linuxcontainers.org/lxd/getting-started-cli/#initial-configuration
        
      lxd_authentication_failure: |-
        The LXD provider could not authenticate to the daemon at %{api_endpoint}.
        
        You may need configure LXD to allow requests from this machine. The
        easiest way to do this is to add your LXC client certificate to LXD's
        list of trusted certificates. This can typically be done with the
        following command:
        
            $ lxc config trust add %{client_cert}
        
        You can find more information about configuring LXD at:

            https://linuxcontainers.org/lxd/getting-started-cli/#initial-configuration
        
      lxd_operation_timeout: |-
        The container failed to respond within %{time_limit} seconds. Try
        running the following command to see whether an error occurs:
        
            $ lxc %{operation} %{machine_id}
        
        If that command runs successfully, there may be a bug in the LXD
        provider. If this is the case, please submit an issue at:
        
            https://gitlab.com/catalyst-it/vagrant-lxd/issues
        
      lxd_network_address_acquisition_timeout: |-
        The container failed to acquire an IPv4 address within %{time_limit}
        seconds. It's possible that the LXD network bridge has not been configured.
        Try running the following command to see whether a bridge exists and has
        an inet address:
        
            $ ip address show %{lxd_bridge}
        
        If that command runs successfully and the bridge appears to be correctly
        configured, there may be a bug in the LXD provider. If this is the case,
        please submit an issue at:
        
            https://gitlab.com/catalyst-it/vagrant-lxd/issues
        
      lxd_container_creation_failure: |-
        The provider was unable to create a container for the '%{machine_name}' VM.
        
        The underlying error message was: %{reason}
        
        The LXD logs may contain more information about the cause of this failure.
        
      lxd_container_configuration_failure: |-
        The provider was unable to configure the container for the '%{machine_name}' VM.
        
        The underlying error message was: %{reason}
        
        The LXD logs may contain more information about the cause of this failure.
        
      lxd_container_deletion_failure: |-
        The provider was unable to delete the container for the '%{machine_name}' VM.

        The underlying error message was: %{reason}
        
        The LXD logs may contain more information about the cause of this failure.
        
      lxd_disk_mount_failure: |-
        The provider was unable to mount a disk device at %{guestpath} for the '%{machine_name}' VM.
        
        The underlying error message was: %{reason}
        
        The LXD logs may contain more information about the cause of this failure.
        
      lxd_disk_unmount_failure: |-
        The provider was unable to unmount a disk device at %{guestpath} for the '%{machine_name}' VM.
        
        The underlying error message was: %{reason}
        
        The LXD logs may contain more information about the cause of this failure.
        
      lxd_image_creation_failure: |-
        The provider was unable to create an LXD image for the '%{machine_name}' VM.
        
        The underlying error message was: %{reason}
        
        This may be a bug in the LXD provider. If you think this is the case,
        please submit an issue at:
        
            https://gitlab.com/catalyst-it/vagrant-lxd/issues
        
      lxd_image_export_failure: |-
        The provider was unable to export a container image for '%{machine_name}'.
        
        The underlying error message was: %{reason}
        
        The LXD logs may contain more information about the cause of this failure.
        
      lxd_duplicate_attachment_failure: |-
        A machine can only be associated with one container at a time.
        
        To attach '%{machine_name}' to '%{container}', you must first
        detach it from its current container using `vagrant lxd detach`
        or pass `--force` to force attachment.
        
      lxd_container_not_found: |-
        The requested container '%{container}' doesn't exist.
        
        You will need to create this container first, either by using the
        `lxc launch` command or by setting the VM's `lxd.name` in its LXD
        provider configuration and running `vagrant up %{machine_name}`.
        
        You can list available containers with the `lxc list` command.
        
      lxd_container_already_exists: |-
        A container with the name '%{container}' already exists.
        
        You will either need to delete this container and try again, or attach
        the VM to it with `vagrant lxd attach %{machine_name} %{container}`.
        
      lxd_certificate_generation_failure: |-
        The LXD provider was unable to generate a client certificate for
        authenticating to the daemon at %{api_endpoint}.
        
        It tried to place the files in %{vagrant_path}.
        
        The underlying error message was: %{reason}
        
        You can use an existing certificate by setting the `client_certificate`
        and `client_key` options in the LXD provider's configuration, or by
        placing 'client.crt' and 'client.key' files in %{default_path}.
        
      snapshot_not_found: |-
        The snapshot name `%{snapshot_name}` was not found for the
        virtual machine `%{machine}`.
