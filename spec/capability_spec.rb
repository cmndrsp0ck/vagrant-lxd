#
# Copyright (c) 2018-2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'fakefs/spec_helpers'

require 'lib/vagrant-lxd'
require 'lib/vagrant-lxd/capability'

describe VagrantLXD::Capability do
  include FakeFS::SpecHelpers

  let(:env) { double('env') }

  let(:valid_subuid) { "root:#{Process.uid}:1" }
  let(:valid_subgid) { "root:#{Process.gid}:1" }

  let(:invalid_subuid) { "root:#{Process.gid + 1}:1" }
  let(:invalid_subgid) { "root:#{Process.gid + 1}:1" }

  subject do
    described_class
  end

  describe 'synced_folders' do
    before do
      Dir.mkdir('/etc')
    end

    context 'with no /etc/subuid' do
      it 'should equal false' do
        File.write('/etc/subgid', valid_subgid)
        subject.synced_folders(env).should be false
      end
    end

    context 'with no /etc/suigid' do
      it 'should equal false' do
        File.write('/etc/subuid', valid_subuid)
        subject.synced_folders(env).should be false
      end
    end

    context 'with no mapping in /etc/subuid' do
      it 'should equal false' do
        File.write('/etc/subuid', invalid_subuid)
        File.write('/etc/subgid', valid_subgid)
        subject.synced_folders(env).should be false
      end
    end

    context 'with no mapping in /etc/subgid' do
      it 'should equal false' do
        File.write('/etc/subuid', valid_subuid)
        File.write('/etc/subgid', invalid_subgid)
        subject.synced_folders(env).should be false
      end
    end

    context 'with mappings in /etc/subuid and /etc/subgid' do
      it 'should equal true' do
        File.write('/etc/subuid', valid_subuid)
        File.write('/etc/subgid', valid_subgid)
        subject.synced_folders(env).should be true
      end
    end
  end
end
