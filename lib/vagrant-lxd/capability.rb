#
# Copyright (c) 2017-2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'vagrant-lxd/config'

module VagrantLXD
  class Capability
    def Capability.snapshot_list(machine)
      env = machine.action(:snapshot_list)
      env[:machine_snapshot_list] || []
    end

    def Capability.synced_folders(env)
      logger = Log4r::Logger.new('vagrant::lxd::capability')
      logger.debug "Checking synced folders support for effective UID/GID #{Process.uid}/#{Process.gid}..."
      result = %w(uid gid).all? do |type|
        begin
          File.readlines("/etc/sub#{type}").grep(/^root:#{Process.send(type)}:[1-9]/).any?
        rescue StandardError => e
          logger.warn "Cannot read subordinate permissions file: #{e.message}"
          false
        end
      end
    end
  end
end
